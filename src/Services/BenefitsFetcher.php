<?php
/**
 * Created by PhpStorm.
 * User: mateu
 * Date: 25.11.2018
 * Time: 00:23
 */

namespace App\Services;


use App\Entity\Benefit;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class BenefitsFetcher
{
    const SOURCE_URL = "http://hackyeahapp.tk/benefits-nfz.csv";

    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    public function sync() {
        $data = $this->fetchData();
        $this->createBenefits($data);
    }

    private function fetchData() {
        return array_map('str_getcsv', file(self::SOURCE_URL));
    }

    private function createBenefits($data) {
        foreach ($data as $datum) {
            $benefit = new Benefit();
            $benefit->setName($datum[0]);
            $this->em->persist($benefit);
        }

        $benefitPOZ = new Benefit();
        $benefitPOZ->setName("opieka internistyczna");
        $this->em->persist($benefitPOZ);
        $this->em->flush();
    }
}