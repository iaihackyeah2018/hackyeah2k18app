<?php
/**
 * Created by PhpStorm.
 * User: mateu
 * Date: 24.11.2018
 * Time: 14:51
 */

namespace App\Services;


use App\Entity\Disease;
use App\Entity\DiseaseName;
use App\Enum\LanguagesEnum;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class ICD10EngCodesFetcher
{
    const SOURCE_URL = "https://raw.githubusercontent.com/kamillamagna/ICD-10-CSV/master/codes.csv";

    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    public function sync() {
        $data = $this->fetchData();
        $this->createDiseases($data);
    }

    private function fetchData() {
        return array_map('str_getcsv', file(self::SOURCE_URL));
    }

    private function createDiseases($data) {
        foreach($data as $datum) {

            $disease = new Disease();
            $disease->setIcd10code($datum[2]);
            $this->em->persist($disease);
            $this->em->flush();

            $diseasesNames = [];
            //ifek na 3 kolumny z nazwami
            for($i = 3; $i <= 5; $i++) {
                $diseaseName = new DiseaseName();
                $diseaseName->setName($datum[$i]);
                $diseaseName->setDisease($disease);
                $diseaseName->setLanguage(LanguagesEnum::ENGLISH);
                $this->em->persist($diseaseName);
                $diseasesNames[] = $diseaseName;
            }
        }
    }
 }