<?php
/**
 * Created by PhpStorm.
 * User: mateu
 * Date: 24.11.2018
 * Time: 23:45
 */

namespace App\Services;


use App\Entity\Benefit;
use App\Entity\Symptom;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class SymptomsFetcher
{
    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    public function createSymptoms() {
        $symptoms = [
            [
                "name" => "ból głowy",
                "benefits" => [ "poradnia neurologiczna" ]
            ],
            [
                "name" => "gorączka",
                "benefits" => [ "oddział chorób wewnętrznych" ]
            ],
            [
                "name" => "ból pleców",
                "benefits" => [ "świadczenia z zakresu ortopedii i traumatologii narządu ruchu",
                    "oddział chirurgii urazowo-ortopedycznej" ]
            ],
            [
                "name" => "kołatanie serca",
                "benefits" => [ "świadczenia z zakresu kardiologii",
                    "oddział kardiologiczny", "oddział kardiologiczny"]
            ],
            [
                "name" => "ból nóg",
                "benefits" => [ "świadczenia z zakresu ortopedii i traumatologii narządu ruchu",
                                "oddział chirurgii urazowo-ortopedycznej"
                    ]
            ],
            [
                "name" => "ból zęba",
                "benefits" => [ "poradnia stomatologiczna" ]
            ],
            [
                "name" => "ból całego ciała",
                "benefits" => [ "poradnia leczenia bólu" ]
            ],
            [
                "name" => "ociężałość",
                "benefits" => [ "poradnia żywieniowa" ]
            ],
            [
                "name" => "katar",
                "benefits" => [ "poradnia otorynolaryngologiczna", "oddział otorynolaryngologiczny" ]
            ]
        ];

        foreach ($symptoms as $symptom) {
            $entity = new Symptom();
            $entity->setName($symptom["name"]);

            $benefitsArray = [];
            foreach($symptom["benefits"] as $benefitsArrayElement) {
                $benefit = $this->em->getRepository(Benefit::class)->findByName($benefitsArrayElement);
                $benefitsArray[] = $benefit;
            }

            $entity->setBenefits($benefit);
            $this->em->persist($entity);
            $this->em->flush();
        }
    }
}