<?php
/**
 * Created by PhpStorm.
 * User: mateu
 * Date: 24.11.2018
 * Time: 14:51
 */

namespace App\Services;


use App\Entity\Disease;
use App\Entity\DiseaseName;
use App\Enum\LanguagesEnum;
use App\Helper\AccentHelper;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class ICD10PlCodesFetcher
{
    const SOURCE_URL = "http://hackyeahapp.tk/icd-10-pl-data.csv";

    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    public function sync() {
        $data = $this->fetchData();
        $this->createDiseases($data);
    }

    private function fetchData() {
/*        $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ); //define bom
        $f = file_get_contents(self::SOURCE_URL); //open the CSV file
#$csv = str_getcsv($f); //it will have bom  这样会出现bom的问题
        $csv = str_getcsv(str_replace($bom,'',$f)); //replace the bom 替换掉bom*/
        $helper = new AccentHelper();
        $test = [];
        $Data = str_getcsv(file_get_contents(self::SOURCE_URL), "\n"); //parse the rows
        foreach($Data as &$Row) {
            $Row = str_getcsv($Row, ";");
            //$Row = array_map([$helper, 'removeAccents'], $Row);
            $test[] = $Row;
        } ; //parse the items in rows

           array_shift($test);

        return $test;
    }

    private function createDiseases($data) {

        foreach($data as $datum) {
            if($datum[11] != "|") {
                $icd10 = $this->parseIcd10WithDotToWithout($datum[11]);
            }
            else $icd10 = $datum[9];

            var_dump($icd10);

            /** @var Disease $disease */
            $disease = $this->em->getRepository(Disease::class)->findOneBy(['icd10code' => $icd10]);

            if($disease == null) continue;

            //nazwa kategorii
            $name = $datum[10];
            var_dump($name);
            //nazwa z poziom 1+2+3
            if($datum[13] != "|") {
                $name = $datum[13]." ".$datum[14]." ".$datum[15];
                $name = str_replace("|", "", $name);
            }
            //nazwa podkategorii
            else if($datum[12] != "|") {
                $name = $datum[12];
            }

            $diseaseName = new DiseaseName();
            $diseaseName->setName($name);
            $diseaseName->setLanguage(LanguagesEnum::POLISH);
            $diseaseName->setDisease($disease);
            $this->em->persist($diseaseName);
            $this->em->flush();
        }
    }

    private function parseIcd10WithDotToWithout($icd10WithDot) {
        return str_replace(".", "", $icd10WithDot);
    }

 }
