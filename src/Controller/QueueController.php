<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class QueueController extends AbstractController
{
    const DOMAIN = 'https://api.nfz.gov.pl';

    /**
     * @Route("/queue", name="queue")
     */
    public function index()
    {
        return $this->render('queue/index.html.twig', [
            'controller_name' => 'QueueController',
        ]);
    }

    /**
     * @param Request $request
     *
     * @Route("/queue/find", name="queue-find")
     */
    public function find(Request $request)
    {
        $headers = [
            'Accept' => 'Application/json'
        ];

        $url = self::DOMAIN . '/queues/' . $request->query->get('id');
        $data = \Unirest\Request::get($url,$headers);
        $data = json_decode($data->raw_body, true);
        $data = $data['data']['attributes'];
        $data['nip_provider'] = $data['nip-provider'];
        $data['statistics']['average_waiting'] = $data['statistics']['average-period'];
        $data['dates']['date_situation_as_at'] = $data['dates']['date-situation-as-at'];

        return $this->render('queue/object.html.twig', ['data' => $data]);
    }
}
