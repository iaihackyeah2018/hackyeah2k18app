<?php
/**
 * Created by PhpStorm.
 * User: mateu
 * Date: 20.11.2018
 * Time: 08:38
 */

namespace App\Controller;


use App\Entity\DiseaseName;
use App\Entity\Symptom;
use App\Enum\SearchEnum;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends AbstractController
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/")
     */
    public function indexAction(Request $request) {
        return $this->render('Home/index.html.twig');
    }

    /**
     * @param Request $request
     * @param string $phrase
     * @Route("find/{phrase}")
     * @return JsonResponse
     */
    public function findAction(Request $request, string $phrase) {
        $em = $this->getDoctrine()->getManager();

        $symptoms = $em->getRepository(Symptom::class)->createQueryBuilder('s')
            ->where('s.name LIKE :phrase')
            ->setParameter('phrase', "%$phrase%")
            ->getQuery()
            ->getResult();

        /** @var Symptom[] $symptoms */
        //$symptoms = $em->getRepository(Symptom::class)->findBy(['name' => $phrase ]);
        /** @var DiseaseName[] $diseasesNames */
        //$diseasesNames = $em->getRepository(DiseaseName::class)->findByName($phrase);
        $diseasesNames = $em->getRepository(DiseaseName::class)->createQueryBuilder('d')
            ->where('d.name LIKE :phrase')
            ->setParameter('phrase', "%$phrase%")
            ->getQuery()
            ->getResult();

        $resultsArray = [];
        $symptomsArray = [];
        foreach ($symptoms as $symptom) {
            $element["id"] = $symptom->getId();
            $element["name"] = $symptom->getName();
            $element["entity"] = SearchEnum::SYMPTOM;
            $symptomsArray[] = $element;
        }

        $diseasesNamesArray = [];
        foreach ($diseasesNames as $diseasesName) {
            $element["id"] = $diseasesName->getId();
            $element["name"] = $diseasesName->getName();
            $element["entity"] = SearchEnum::DISEASE;
            $diseasesNamesArray[] = $element;
        }

        $resultsArray = array_merge($symptomsArray, $diseasesNamesArray);

        return new JsonResponse(array_slice($resultsArray, 0, 10));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("addSymptom")
     */
    public function addSymptomAction(Request $request) {
        return $this->render('Home/addSymptom.html.twig');
    }
}