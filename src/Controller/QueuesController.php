<?php

namespace App\Controller;

use App\Entity\Benefit;
use App\Entity\Disease;
use App\Entity\Symptom;
use App\Enum\ModeEnum;
use App\Enum\SearchEnum;
use Couchbase\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class QueuesController extends AbstractController
{
    const DOMAIN = 'https://api.nfz.gov.pl';
    /**
     * @Route("/queues", name="queues")
     */
    public function index()
    {
        return $this->render('queues/index.html.twig', [
            'controller_name' => 'QueuesController',
        ]);
    }

    /**
     * @param Request $request
     * @Route("/queues/find/{mode}/{entity}/{id}", name="find")
     * @return Response
     * @throws \Exception
     */
    public function findByBenefit(Request $request, string $mode, int $entity, int $id)
    {
        $em = $this->getDoctrine()->getManager();
        if($entity == SearchEnum::SYMPTOM) {
            $symptoms = $em->getRepository(Symptom::class)->findById($id);
            $benefits = $symptoms[0]->getBenefits();
            $benefit = $benefits[0];
            //$benefit = $em->getRepository(Benefit::class)->findOneBy(['symptoms' => $symptoms ]);
        }
        else if($entity == SearchEnum::DISEASE) {
            //do naprawienia
            $diseases = $em->getRepository(Disease::class)->findById($id);
            $benefits = $diseases[0]->getBenefits();
            $benefit = $benefits[0];
            //$benefit = $em->getRepository(Benefit::class)->findOneBy(['diseases' => $diseases ]);
        }
        else throw new \Exception();

        if($benefit == null) {
            return $this->render('queues/noBenefits.html.twig', [ 'allBenefits' => $em->getRepository(Benefit::class)->findAll() ] );
        }

        $filteredData = $this->getData($request, $benefit);

        if ($mode == ModeEnum::LIST) {
            return $this->render('queues/findByBenefitList.html.twig', ['data' => $filteredData, 'query' => $benefit, 'id' => $id, 'entity' => $entity]);
        } else if ($mode == ModeEnum::MAP) {
            return $this->render('queues/findByBenefit.html.twig', ['data' => $filteredData, 'query' => $benefit, 'id' => $id, 'entity' => $entity]);
        }
        else new \Exception();

        return new Response();
    }

    function fillData($item, Request $request)
    {
        $element = [];
        $element['id'] = $item['id'];
        $element['provider'] = $item['attributes']['provider'];
        $element['phone'] = $item['attributes']['phone'];
        $element['address'] = $item['attributes']['address'];
        $element['city'] = $item['attributes']['locality'];
        $element['statistics'] = $item['attributes']['statistics'];
        $element['coordinates']['lat'] = $item['attributes']['latitude'];
        $element['coordinates']['lng'] = $item['attributes']['longitude'];
        $element['first_available_date'] = $item['attributes']['dates']['date'];
        $element['availability_check_date'] = $item['attributes']['dates']['date-situation-as-at'];
        $element['dist'] = $this->calculateDistance($request->cookies->get('lat'),$request->cookies->get('lng'),$element['coordinates']['lat'], $element['coordinates']['lng']);

        return $element;
    }

    function getData(Request $request, Benefit $benefit) {
        $headers = [
            'Accept' => 'Application/json'
        ];
        $url = self::DOMAIN . '/queues?page=1&limit=25&format=json&case=1&benefit=' . $benefit->getName();
        $data = \Unirest\Request::get($url,$headers);
        $data = json_decode($data->raw_body, true);
        $filteredData = [];
        foreach ($data['data'] as $item) {
            $filteredData[] = $this->fillData($item, $request);
        }
        $cnt = 0;
        if ($data['meta']['count'] > 250) {
            $useCity = true;

//            if ($request->cookies->get('town') == 'undefined') {
//                $city = $request->cookies->get('city');
//            } else {
//                $city = $request->cookies->get('town');
//            }
            $state = $this->convertStateToId($request);
            $filteredData = [];
        } else $useCity = false;
        while($data['links']['next'] != null) {
            $url = self::DOMAIN . $data['links']['next'] . ($useCity == true ? '&province=' . $state : '');
            $data = \Unirest\Request::get($url,$headers);
            $data = json_decode($data->raw_body, true);
            foreach ($data['data'] as $item) {
                $filteredData[] = $this->fillData($item, $request);
            }
            if ($cnt++ > 10) break;
        }

        return $filteredData;
    }

    function convertStateToId(Request $request) {
        return '07';

        switch ($request->cookies->get('state')) {
            case 'województwo wielkopolskie':
                return '15';

            case 'województwo mazowieckie':
                return '07';
        }

        return null;
    }

    public function calculateDistance($lat1, $lon1, $lat2, $lon2)
    {
        // 1 - from, 2 - to
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $km = $miles * 1.609344;
        $km = ($km > 500) ? $km / 10 : $km;
        $distance = $km .' km';

        return $distance;
    }
}
