<?php

namespace App\Controller;

use App\Entity\Ambulance;
use App\Entity\Location;
use Egulias\EmailValidator\Exception\ExpectingCTEXT;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class AmbulanceController extends AbstractController
{
    /** @var Request */
    private $request;

    const FILE_AMBULANCES =   'ambulances.dat';
    /**
     * @return Location
     *
     * @throws \Exception
     */
    private function getCurrentLocationDataFromParams()
    {
        if (!$this->request) {
            throw new \Exception('No request data detected');
        }
        return new Location(
            [
                'lat' => $this->request->get('lat'),
                'lng' => $this->request->get('lng')
            ]
        );
    }

    private  function saveNearestToSession(Ambulance $ambulance)
    {
        $session = new Session();
        $session->set('nearestAmbulance', $ambulance);
        $session->save();
    }

     static function rand_float($st_num = 0, $end_num = 1, $mul = 1000000)
    {
        if ($st_num > $end_num) return false;
        return mt_rand($st_num * $mul, $end_num * $mul) / $mul;
    }

    /**
     * Calculates the great-circle distance between two points, with
     * the Haversine formula.
     * @param float $latitudeFrom Latitude of start point in [deg decimal]
     * @param float $longitudeFrom Longitude of start point in [deg decimal]
     * @param float $latitudeTo Latitude of target point in [deg decimal]
     * @param float $longitudeTo Longitude of target point in [deg decimal]
     * @param float $earthRadius Mean earth radius in [m]
     * @return float Distance between points in [m] (same as earthRadius)
     */
    function haversineGreatCircleDistance(
        $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
    {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        return $angle * $earthRadius;
    }

    /**
     * @param Location $location
     * @return array
     * @throws \Exception
     */
    private  function mockAmbulanceData(Location $location)
    {
        if (!file_exists(self::FILE_AMBULANCES)) {
            $ambulances = $this->getDoctrine()->getRepository(Ambulance::class)->findAll();

            @file_put_contents(self::FILE_AMBULANCES, serialize($ambulances));
        } else {
            $ambulances = unserialize(file_get_contents(self::FILE_AMBULANCES));
        }
//        var_dump(unserialize(serialize($ambulances)));die;
//        file_put_contents(self::FILE_AMBULANCES, serialize($ambulances));
        $time = [];
        $data = [];
        foreach ($ambulances as $ambulance) {
            $distance = $this->haversineGreatCircleDistance($location->getLat(), $location->getLng(), $ambulance->getLat(), $ambulance->getLng());
            if ($distance > 50000) {//50km
                continue;
            }
            $travelData = self::getTimeTravelToAmbulance($location, new Location([
                'lat' => $ambulance->getLat(),
                'lng' => $ambulance->getLng()
            ]));
            $time[$ambulance->getId()] = $travelData['seconds'];
            $ambulance->setTimeTravelData($travelData);
            $ambulance->setPhone(str_replace([' ', '-'], '', $ambulance->getPhone()));
            $data[$ambulance->getId()] = $ambulance;
        }
        $data[array_search(max($time), $time)]->setColor('red');
        $ambulanceNearest = $data[array_search(min($time), $time)]->setColor('green');
        self::saveNearestToSession($ambulanceNearest);

        return $data;
    }

    /**
     * @param Location $from
     * @param Location $to
     *
     * @return array
     */
    private  function getTimeTravelToAmbulance(Location $from, Location $to): array
    {
        $url = 'https://maps.googleapis.com/maps/api/distancematrix/json?';
        $url .= 'origins=' . $from->getLat() . ',' . $from->getLng() . '&destinations=' . $to->getLat() . ',' . $to->getLng() . '&mode=driving&language=pl-PL&key=AIzaSyD36EpZR-H_zl30MbU0q2C8Vk8L4fkCzd0';
        $data = json_decode(file_get_contents($url), true);
        return [
            'text' => $data['rows'][0]['elements'][0]['duration']['text'],
            'seconds' => $data['rows'][0]['elements'][0]['duration']['value'],
            'distance' => $data['rows'][0]['elements'][0]['distance']['text'],
            'color' => 'blue'
        ];
    }

    /**
     * @param Location $location
     * @return array
     * @throws \Exception
     */
    private function getNearAmbulanceData(Location $location)
    {
        $data = $this->mockAmbulanceData($location);

        return $data;
    }

    /**
     * Akcja do pobrania mapy z ambulansami
     *
     * @Route("/ambulance/get-map-content/{lat}/{lng}", name="get-map-content")
     */
    public function getMapContentAction(Request $request)
    {
        $this->request = $request;
        try {
            $yourLocation = $this->getCurrentLocationDataFromParams();
            $locations = $this->getNearAmbulanceData($yourLocation);
            return $this->render('ambulance/get-map-content.html.twig', [
                'lat' => $request->get('lat'),
                'lng' => $request->get('lng'),
                'ambulances' => $locations
            ]);
        } catch (\Exception $e) {
            return $this->render('ambulance/error.html.twig', [
                'reasonMessage' => $e->getMessage()
            ]);
        }
    }

    /**
     * Widok główny
     *
     * @Route("/ambulance/get-map", name="ambulance-get-map")
     */
    public function getMapAction()
    {
        return $this->render('ambulance/get-map.html.twig');
    }

    /**
     * Najlbliższa
     *
     * @Route("/ambulance/get-nearest", name="ambulance-get-nearest")
     */
    public function getNearestAction()
    {
        $session = new Session();
        $ambulance = $session->get('nearestAmbulance');
        return $this->render('ambulance/nearest.html.twig', [
            'ambulance' => $ambulance
        ]);
    }

//    /**
//     * @Route("/ambulance/load", name="load")
//     */
//    public function load()
//    {
//        set_time_limit(36000);
//        $xml = simplexml_load_string(file_get_contents('http://localhost/hackyeah/public/rejestr.xml'));
//
//        $xml = json_decode(json_encode($xml), true);
//        $i = 0;
//        $errors = [];
////        foreach ($xml as $x) {
////            $jednostki = $x['jednostka'];
//        $jednostki = array(0 => array('nazwaPodmiotu' => 'POWIATOWA STACJA POGOTOWIA RATUNKOWEGO SAMODZIELNY PUBLICZNY ZAKŁAD W MIELCU', 'nazwaJednostki' => 'Specjalistyczny Zespół Ratownictwa Medycznego R1201', 'adresPodmiotu' => array('wojewodztwo' => 'PODKARPACKIE', 'powiat' => 'mielecki', 'gmina' => 'Mielec', 'miejscowosc' => 'Mielec', 'kodPocztowy' => '39-300', 'rodzaj' => 'ul.', 'ulica' => 'Żeromskiego', 'budynek' => '22', 'kodTeryt' => '1811011', 'kodMiejscowosci' => '0974618', 'kodUlicy' => '26464',), 'adresJednostki' => array('wojewodztwo' => 'PODKARPACKIE', 'powiat' => 'mielecki', 'gmina' => 'Mielec', 'miejscowosc' => 'Mielec', 'kodPocztowy' => '39-300', 'rodzaj' => 'ul.', 'ulica' => 'Żeromskiego', 'budynek' => '22', 'kodTeryt' => '1811011', 'kodMiejscowosci' => '0974618', 'kodUlicy' => '26464',), 'numerWpisu' => '00040/18/2017', 'dataWpisu' => '2017-12-06', 'telefon' => '17 773 63 01', 'email' => 'pspr@pogotowie-mielec.pl', 'dataObowiazywaniaOd' => '2018-09-04', 'dataObowiazywaniaDo' => '2018-09-04', 'kodSpecjalnosci' => '3114', 'rodzajSpecjalnosci' => 'Zespół ratownictwa medycznego specjalistyczny', 'miejcaStacjonowania' => array('miejsceStacjonowania' => array('kodZespolu' => '1811011401', 'typZespolu' => 'Specjalistyczny', 'okresGotowosci' => '01.01 - 31.12',),),), 1 => array('nazwaPodmiotu' => 'POWIATOWA STACJA POGOTOWIA RATUNKOWEGO SAMODZIELNY PUBLICZNY ZAKŁAD W MIELCU', 'nazwaJednostki' => 'Podstawowy Zespół Ratownictwa Medycznego R1202', 'adresPodmiotu' => array('wojewodztwo' => 'PODKARPACKIE', 'powiat' => 'mielecki', 'gmina' => 'Mielec', 'miejscowosc' => 'Mielec', 'kodPocztowy' => '39-300', 'rodzaj' => 'ul.', 'ulica' => 'Żeromskiego', 'budynek' => '22', 'kodTeryt' => '1811011', 'kodMiejscowosci' => '0974618', 'kodUlicy' => '26464',), 'adresJednostki' => array('wojewodztwo' => 'PODKARPACKIE', 'powiat' => 'mielecki', 'gmina' => 'Mielec', 'miejscowosc' => 'Mielec', 'kodPocztowy' => '39-300', 'rodzaj' => 'ul.', 'ulica' => 'Żeromskiego', 'budynek' => '22', 'kodTeryt' => '1811011', 'kodMiejscowosci' => '0974618', 'kodUlicy' => '26464',), 'numerWpisu' => '00041/18/2017', 'dataWpisu' => '2017-12-06', 'telefon' => '17 773 63 01', 'email' => 'pspr@pogotowie-mielec.pl', 'dataObowiazywaniaOd' => '2018-09-04', 'dataObowiazywaniaDo' => '2018-09-04', 'kodSpecjalnosci' => '3112', 'rodzajSpecjalnosci' => 'Zespół ratownictwa medycznego podstawowy', 'miejcaStacjonowania' => array('miejsceStacjonowania' => array('kodZespolu' => '1811011201', 'typZespolu' => 'Podstawowy', 'okresGotowosci' => '01.01 - 31.12',),),), 2 => array('nazwaPodmiotu' => 'POWIATOWA STACJA POGOTOWIA RATUNKOWEGO SAMODZIELNY PUBLICZNY ZAKŁAD W MIELCU', 'nazwaJednostki' => 'Podstawowy Zespół Ratownictwa Medycznego R1208', 'adresPodmiotu' => array('wojewodztwo' => 'PODKARPACKIE', 'powiat' => 'mielecki', 'gmina' => 'Mielec', 'miejscowosc' => 'Mielec', 'kodPocztowy' => '39-300', 'rodzaj' => 'ul.', 'ulica' => 'Żeromskiego', 'budynek' => '22', 'kodTeryt' => '1811011', 'kodMiejscowosci' => '0974618', 'kodUlicy' => '26464',), 'adresJednostki' => array('wojewodztwo' => 'PODKARPACKIE', 'powiat' => 'mielecki', 'gmina' => 'Mielec', 'miejscowosc' => 'Mielec', 'kodPocztowy' => '39-300', 'rodzaj' => 'ul.', 'ulica' => 'Żeromskiego', 'budynek' => '22', 'kodTeryt' => '1811011', 'kodMiejscowosci' => '0974618', 'kodUlicy' => '26464',), 'numerWpisu' => '00042/18/2017', 'dataWpisu' => '2017-12-06', 'telefon' => '17 773 63 01', 'email' => 'pspr@pogotowie-mielec.pl', 'dataObowiazywaniaOd' => '2018-09-04', 'dataObowiazywaniaDo' => '2018-09-04', 'kodSpecjalnosci' => '3112', 'rodzajSpecjalnosci' => 'Zespół ratownictwa medycznego podstawowy', 'miejcaStacjonowania' => array('miejsceStacjonowania' => array('kodZespolu' => '1811011204', 'typZespolu' => 'Podstawowy', 'okresGotowosci' => '01.01 - 31.12',),),), 3 => array('nazwaPodmiotu' => 'POWIATOWA STACJA POGOTOWIA RATUNKOWEGO SAMODZIELNY PUBLICZNY ZAKŁAD W MIELCU', 'nazwaJednostki' => 'Podstawowy Zespół Ratownictwa Medycznego R1204', 'adresPodmiotu' => array('wojewodztwo' => 'PODKARPACKIE', 'powiat' => 'mielecki', 'gmina' => 'Mielec', 'miejscowosc' => 'Mielec', 'kodPocztowy' => '39-300', 'rodzaj' => 'ul.', 'ulica' => 'Żeromskiego', 'budynek' => '22', 'kodTeryt' => '1811011', 'kodMiejscowosci' => '0974618', 'kodUlicy' => '26464',), 'adresJednostki' => array('wojewodztwo' => 'PODKARPACKIE', 'powiat' => 'mielecki', 'gmina' => 'Borowa', 'miejscowosc' => 'Borowa', 'kodPocztowy' => '39-305', 'budynek' => '333A', 'kodTeryt' => '1811022', 'kodMiejscowosci' => '0645458',), 'numerWpisu' => '00043/18/2017', 'dataWpisu' => '2017-12-06', 'telefon' => '17 773 63 01', 'email' => 'pspr@pogotowie-mielec.pl', 'dataObowiazywaniaOd' => '2018-09-04', 'dataObowiazywaniaDo' => '2018-09-04', 'kodSpecjalnosci' => '3112', 'rodzajSpecjalnosci' => 'Zespół ratownictwa medycznego podstawowy', 'miejcaStacjonowania' => array('miejsceStacjonowania' => array('kodZespolu' => '1811022202', 'typZespolu' => 'Podstawowy', 'okresGotowosci' => '01.01 - 31.12',),),), 4 => array('nazwaPodmiotu' => 'POWIATOWA STACJA POGOTOWIA RATUNKOWEGO SAMODZIELNY PUBLICZNY ZAKŁAD W MIELCU', 'nazwaJednostki' => 'Podstawowy Zespół Ratownictwa Medycznego R1206', 'adresPodmiotu' => array('wojewodztwo' => 'PODKARPACKIE', 'powiat' => 'mielecki', 'gmina' => 'Mielec', 'miejscowosc' => 'Mielec', 'kodPocztowy' => '39-300', 'rodzaj' => 'ul.', 'ulica' => 'Żeromskiego', 'budynek' => '22', 'kodTeryt' => '1811011', 'kodMiejscowosci' => '0974618', 'kodUlicy' => '26464',), 'adresJednostki' => array('wojewodztwo' => 'PODKARPACKIE', 'powiat' => 'mielecki', 'gmina' => 'Radomyśl Wielki', 'miejscowosc' => 'Radomyśl Wielki', 'kodPocztowy' => '39-310', 'rodzaj' => 'ul.', 'ulica' => 'Armii Krajowej', 'budynek' => '5', 'kodTeryt' => '1811084', 'kodMiejscowosci' => '0982411', 'kodUlicy' => '00432',), 'numerWpisu' => '00044/18/2017', 'dataWpisu' => '2017-12-06', 'telefon' => '17 773 63 01', 'email' => 'pspr@pogotowie-mielec.pl', 'dataObowiazywaniaOd' => '2018-09-04', 'dataObowiazywaniaDo' => '2018-09-04', 'kodSpecjalnosci' => '3112', 'rodzajSpecjalnosci' => 'Zespół ratownictwa medycznego podstawowy', 'miejcaStacjonowania' => array('miejsceStacjonowania' => array('kodZespolu' => '1811084203', 'typZespolu' => 'Podstawowy', 'okresGotowosci' => '01.01 - 31.12',),),), 5 => array('nazwaPodmiotu' => 'POWIATOWA STACJA POGOTOWIA RATUNKOWEGO SAMODZIELNY PUBLICZNY ZAKŁAD W MIELCU', 'nazwaJednostki' => 'Podstawowy Zespół Ratownictwa Medycznego R1206', 'adresPodmiotu' => array('wojewodztwo' => 'PODKARPACKIE', 'powiat' => 'mielecki', 'gmina' => 'Mielec', 'miejscowosc' => 'Mielec', 'kodPocztowy' => '39-300', 'rodzaj' => 'ul.', 'ulica' => 'Żeromskiego', 'budynek' => '22', 'kodTeryt' => '1811011', 'kodMiejscowosci' => '0974618', 'kodUlicy' => '26464',), 'adresJednostki' => array('wojewodztwo' => 'PODKARPACKIE', 'powiat' => 'mielecki', 'gmina' => 'Padew Narodowa', 'miejscowosc' => 'Padew Narodowa', 'kodPocztowy' => '39-340', 'rodzaj' => 'ul.', 'ulica' => 'Ludwiki Uzar-Krysiakowej', 'budynek' => '20', 'kodTeryt' => '1811062', 'kodMiejscowosci' => '0803160', 'kodUlicy' => '41960',), 'numerWpisu' => '00045/18/2017', 'dataWpisu' => '2017-12-06', 'telefon' => '17 773 63 01', 'email' => 'pspr@pogotowie-mielec.pl', 'dataObowiazywaniaOd' => '2018-09-04', 'dataObowiazywaniaDo' => '2018-09-04', 'kodSpecjalnosci' => '3112', 'rodzajSpecjalnosci' => 'Zespół ratownictwa medycznego podstawowy', 'miejcaStacjonowania' => array('miejsceStacjonowania' => array('kodZespolu' => '1811062205', 'typZespolu' => 'Podstawowy', 'okresGotowosci' => '01.01 - 31.12',),),), 6 => array('nazwaPodmiotu' => 'SZPITAL POWIATOWY IM.EDMUNDA BIERNACKIEGO W MIELCU', 'nazwaJednostki' => 'Szpitalny Oddział Ratunkowy', 'adresPodmiotu' => array('wojewodztwo' => 'PODKARPACKIE', 'powiat' => 'mielecki', 'gmina' => 'Mielec', 'miejscowosc' => 'Mielec', 'kodPocztowy' => '39-300', 'rodzaj' => 'ul.', 'ulica' => 'Żeromskiego', 'budynek' => '22', 'kodTeryt' => '1811011', 'kodMiejscowosci' => '0974618', 'kodUlicy' => '26464',), 'adresJednostki' => array('wojewodztwo' => 'PODKARPACKIE', 'powiat' => 'mielecki', 'gmina' => 'Mielec', 'miejscowosc' => 'Mielec', 'kodPocztowy' => '39-300', 'rodzaj' => 'ul.', 'ulica' => 'Żeromskiego', 'budynek' => '22', 'kodTeryt' => '1811011', 'kodMiejscowosci' => '0974618', 'kodUlicy' => '26464',), 'numerWpisu' => '00095/18/2017', 'dataWpisu' => '2017-12-14', 'telefon' => '17 780 01 17', 'email' => 'sor@szpital.mielec.pl', 'dataObowiazywaniaOd' => '2017-12-14', 'dataObowiazywaniaDo' => '2017-12-14', 'kodSpecjalnosci' => '4902', 'rodzajSpecjalnosci' => 'Szpitalny oddział ratunkowy', 'miejcaStacjonowania' => array(),), 7 => array('nazwaPodmiotu' => 'SZPITAL POWIATOWY IM.EDMUNDA BIERNACKIEGO W MIELCU', 'nazwaJednostki' => 'Oddział Chirurgii Ogólnej', 'adresPodmiotu' => array('wojewodztwo' => 'PODKARPACKIE', 'powiat' => 'mielecki', 'gmina' => 'Mielec', 'miejscowosc' => 'Mielec', 'kodPocztowy' => '39-300', 'rodzaj' => 'ul.', 'ulica' => 'Żeromskiego', 'budynek' => '22', 'kodTeryt' => '1811011', 'kodMiejscowosci' => '0974618', 'kodUlicy' => '26464',), 'adresJednostki' => array('wojewodztwo' => 'PODKARPACKIE', 'powiat' => 'mielecki', 'gmina' => 'Mielec', 'miejscowosc' => 'Mielec', 'kodPocztowy' => '39-300', 'rodzaj' => 'ul.', 'ulica' => 'Żeromskiego', 'budynek' => '22', 'kodTeryt' => '1811011', 'kodMiejscowosci' => '0974618', 'kodUlicy' => '26464',), 'numerWpisu' => '00187/18/2017', 'dataWpisu' => '2017-12-19', 'telefon' => '17 780 01 63', 'email' => 'o.chirurgii@szpital.mielec.pl', 'dataObowiazywaniaOd' => '2017-12-19', 'dataObowiazywaniaDo' => '2017-12-19', 'kodSpecjalnosci' => '4500', 'rodzajSpecjalnosci' => 'Oddział chirurgiczny ogólny', 'miejcaStacjonowania' => array(),), 8 => array('nazwaPodmiotu' => 'SZPITAL POWIATOWY IM.EDMUNDA BIERNACKIEGO W MIELCU', 'nazwaJednostki' => 'Oddział Chirurgii Urazowo-Ortopedycznej', 'adresPodmiotu' => array('wojewodztwo' => 'PODKARPACKIE', 'powiat' => 'mielecki', 'gmina' => 'Mielec', 'miejscowosc' => 'Mielec', 'kodPocztowy' => '39-300', 'rodzaj' => 'ul.', 'ulica' => 'Żeromskiego', 'budynek' => '22', 'kodTeryt' => '1811011', 'kodMiejscowosci' => '0974618', 'kodUlicy' => '26464',), 'adresJednostki' => array('wojewodztwo' => 'PODKARPACKIE', 'powiat' => 'mielecki', 'gmina' => 'Mielec', 'miejscowosc' => 'Mielec', 'kodPocztowy' => '39-300', 'rodzaj' => 'ul.', 'ulica' => 'Żeromskiego', 'budynek' => '22', 'kodTeryt' => '1811011', 'kodMiejscowosci' => '0974618', 'kodUlicy' => '26464',), 'numerWpisu' => '00188/18/2017', 'dataWpisu' => '2017-12-19', 'telefon' => '17 780 03 00', 'email' => 'o.ortopedyczny@szpital.mielec.pl', 'dataObowiazywaniaOd' => '2017-12-19', 'dataObowiazywaniaDo' => '2017-12-19', 'kodSpecjalnosci' => '4580', 'rodzajSpecjalnosci' => 'Oddział chirurgii urazowo-ortopedycznej', 'miejcaStacjonowania' => array(),), 9 => array('nazwaPodmiotu' => 'SZPITAL POWIATOWY IM.EDMUNDA BIERNACKIEGO W MIELCU', 'nazwaJednostki' => 'Oddział Neurologii', 'adresPodmiotu' => array('wojewodztwo' => 'PODKARPACKIE', 'powiat' => 'mielecki', 'gmina' => 'Mielec', 'miejscowosc' => 'Mielec', 'kodPocztowy' => '39-300', 'rodzaj' => 'ul.', 'ulica' => 'Żeromskiego', 'budynek' => '22', 'kodTeryt' => '1811011', 'kodMiejscowosci' => '0974618', 'kodUlicy' => '26464',), 'adresJednostki' => array('wojewodztwo' => 'PODKARPACKIE', 'powiat' => 'mielecki', 'gmina' => 'Mielec', 'miejscowosc' => 'Mielec', 'kodPocztowy' => '39-300', 'rodzaj' => 'ul.', 'ulica' => 'Żeromskiego', 'budynek' => '22', 'kodTeryt' => '1811011', 'kodMiejscowosci' => '0974618', 'kodUlicy' => '26464',), 'numerWpisu' => '00189/18/2017', 'dataWpisu' => '2017-12-19', 'telefon' => '17 780 03 14', 'email' => 'o.neurologiczny@szpital.mielec.pl', 'dataObowiazywaniaOd' => '2017-12-19', 'dataObowiazywaniaDo' => '2017-12-19', 'kodSpecjalnosci' => '4220', 'rodzajSpecjalnosci' => 'Oddział neurologiczny', 'miejcaStacjonowania' => array(),), 10 => array('nazwaPodmiotu' => 'SZPITAL POWIATOWY IM.EDMUNDA BIERNACKIEGO W MIELCU', 'nazwaJednostki' => 'Oddział Chorób Wewnętrznych i Kardiologii', 'adresPodmiotu' => array('wojewodztwo' => 'PODKARPACKIE', 'powiat' => 'mielecki', 'gmina' => 'Mielec', 'miejscowosc' => 'Mielec', 'kodPocztowy' => '39-300', 'rodzaj' => 'ul.', 'ulica' => 'Żeromskiego', 'budynek' => '22', 'kodTeryt' => '1811011', 'kodMiejscowosci' => '0974618', 'kodUlicy' => '26464',), 'adresJednostki' => array('wojewodztwo' => 'PODKARPACKIE', 'powiat' => 'mielecki', 'gmina' => 'Mielec', 'miejscowosc' => 'Mielec', 'kodPocztowy' => '39-300', 'rodzaj' => 'ul.', 'ulica' => 'Żeromskiego', 'budynek' => '22', 'kodTeryt' => '1811011', 'kodMiejscowosci' => '0974618', 'kodUlicy' => '26464',), 'numerWpisu' => '00190/18/2017', 'dataWpisu' => '2017-12-19', 'telefon' => '17 780 03 37', 'email' => 'o.wewnetrzyny@szpital.mielec.pl', 'dataObowiazywaniaOd' => '2017-12-19', 'dataObowiazywaniaDo' => '2017-12-19', 'kodSpecjalnosci' => '4000', 'rodzajSpecjalnosci' => 'Oddział chorób wewnętrznych', 'miejcaStacjonowania' => array(),), 11 => array('nazwaPodmiotu' => 'SZPITAL POWIATOWY IM.EDMUNDA BIERNACKIEGO W MIELCU', 'nazwaJednostki' => 'Oddział Udarowy', 'adresPodmiotu' => array('wojewodztwo' => 'PODKARPACKIE', 'powiat' => 'mielecki', 'gmina' => 'Mielec', 'miejscowosc' => 'Mielec', 'kodPocztowy' => '39-300', 'rodzaj' => 'ul.', 'ulica' => 'Żeromskiego', 'budynek' => '22', 'kodTeryt' => '1811011', 'kodMiejscowosci' => '0974618', 'kodUlicy' => '26464',), 'adresJednostki' => array('wojewodztwo' => 'PODKARPACKIE', 'powiat' => 'mielecki', 'gmina' => 'Mielec', 'miejscowosc' => 'Mielec', 'kodPocztowy' => '39-300', 'rodzaj' => 'ul.', 'ulica' => 'Żeromskiego', 'budynek' => '22', 'kodTeryt' => '1811011', 'kodMiejscowosci' => '0974618', 'kodUlicy' => '26464',), 'numerWpisu' => '00191/18/2017', 'dataWpisu' => '2017-12-19', 'telefon' => '17 780 03 14', 'email' => 'o.neurologiczny@szpital.mielec.pl', 'dataObowiazywaniaOd' => '2017-12-19', 'dataObowiazywaniaDo' => '2017-12-19', 'kodSpecjalnosci' => '4222', 'rodzajSpecjalnosci' => 'Oddział udarowy', 'miejcaStacjonowania' => array(),), 12 => array('nazwaPodmiotu' => 'SZPITAL POWIATOWY IM.EDMUNDA BIERNACKIEGO W MIELCU', 'nazwaJednostki' => 'Oddział Obserwacyjno-Zakaźny i Chorób Wątroby', 'adresPodmiotu' => array('wojewodztwo' => 'PODKARPACKIE', 'powiat' => 'mielecki', 'gmina' => 'Mielec', 'miejscowosc' => 'Mielec', 'kodPocztowy' => '39-300', 'rodzaj' => 'ul.', 'ulica' => 'Żeromskiego', 'budynek' => '22', 'kodTeryt' => '1811011', 'kodMiejscowosci' => '0974618', 'kodUlicy' => '26464',), 'adresJednostki' => array('wojewodztwo' => 'PODKARPACKIE', 'powiat' => 'mielecki', 'gmina' => 'Mielec', 'miejscowosc' => 'Mielec', 'kodPocztowy' => '39-300', 'rodzaj' => 'ul.', 'ulica' => 'Żeromskiego', 'budynek' => '22', 'kodTeryt' => '1811011', 'kodMiejscowosci' => '0974618', 'kodUlicy' => '26464',), 'numerWpisu' => '00192/18/2017', 'dataWpisu' => '2017-12-19', 'telefon' => '17 780 03 91', 'email' => 'o.zakazny@szpital.mielec.pl', 'dataObowiazywaniaOd' => '2017-12-19', 'dataObowiazywaniaDo' => '2017-12-19', 'kodSpecjalnosci' => '4348', 'rodzajSpecjalnosci' => 'Oddział obserwacyjno-zakaźny', 'miejcaStacjonowania' => array(),), 13 => array('nazwaPodmiotu' => 'SZPITAL POWIATOWY IM.EDMUNDA BIERNACKIEGO W MIELCU', 'nazwaJednostki' => 'Oddział Neurochirurgii', 'adresPodmiotu' => array('wojewodztwo' => 'PODKARPACKIE', 'powiat' => 'mielecki', 'gmina' => 'Mielec', 'miejscowosc' => 'Mielec', 'kodPocztowy' => '39-300', 'rodzaj' => 'ul.', 'ulica' => 'Żeromskiego', 'budynek' => '22', 'kodTeryt' => '1811011', 'kodMiejscowosci' => '0974618', 'kodUlicy' => '26464',), 'adresJednostki' => array('wojewodztwo' => 'PODKARPACKIE', 'powiat' => 'mielecki', 'gmina' => 'Mielec', 'miejscowosc' => 'Mielec', 'kodPocztowy' => '39-300', 'rodzaj' => 'ul.', 'ulica' => 'Żeromskiego', 'budynek' => '22', 'kodTeryt' => '1811011', 'kodMiejscowosci' => '0974618', 'kodUlicy' => '26464',), 'numerWpisu' => '00193/18/2017', 'dataWpisu' => '2017-12-19', 'telefon' => '17 780 03 52', 'email' => 'o.neurochirurgii@szpital.mielec.pl', 'dataObowiazywaniaOd' => '2017-12-19', 'dataObowiazywaniaDo' => '2017-12-19', 'kodSpecjalnosci' => '4570', 'rodzajSpecjalnosci' => 'Oddział neurochirurgiczny', 'miejcaStacjonowania' => array(),), 14 => array('nazwaPodmiotu' => 'SZPITAL POWIATOWY IM.EDMUNDA BIERNACKIEGO W MIELCU', 'nazwaJednostki' => 'Oddział Urologii Ogólnej i Onkologicznej', 'adresPodmiotu' => array('wojewodztwo' => 'PODKARPACKIE', 'powiat' => 'mielecki', 'gmina' => 'Mielec', 'miejscowosc' => 'Mielec', 'kodPocztowy' => '39-300', 'rodzaj' => 'ul.', 'ulica' => 'Żeromskiego', 'budynek' => '22', 'kodTeryt' => '1811011', 'kodMiejscowosci' => '0974618', 'kodUlicy' => '26464',), 'adresJednostki' => array('wojewodztwo' => 'PODKARPACKIE', 'powiat' => 'mielecki', 'gmina' => 'Mielec', 'miejscowosc' => 'Mielec', 'kodPocztowy' => '39-300', 'rodzaj' => 'ul.', 'ulica' => 'Żeromskiego', 'budynek' => '22', 'kodTeryt' => '1811011', 'kodMiejscowosci' => '0974618', 'kodUlicy' => '26464',), 'numerWpisu' => '00194/18/2017', 'dataWpisu' => '2017-12-19', 'telefon' => '17 780 03 73', 'email' => 'o.urologiczny@szpital.mielec.pl', 'dataObowiazywaniaOd' => '2017-12-19', 'dataObowiazywaniaDo' => '2017-12-19', 'kodSpecjalnosci' => '4640', 'rodzajSpecjalnosci' => 'Oddział urologiczny', 'miejcaStacjonowania' => array(),), 15 => array('nazwaPodmiotu' => 'SZPITAL POWIATOWY IM.EDMUNDA BIERNACKIEGO W MIELCU', 'nazwaJednostki' => 'Oddział Ginekologiczno-Położniczy', 'adresPodmiotu' => array('wojewodztwo' => 'PODKARPACKIE', 'powiat' => 'mielecki', 'gmina' => 'Mielec', 'miejscowosc' => 'Mielec', 'kodPocztowy' => '39-300', 'rodzaj' => 'ul.', 'ulica' => 'Żeromskiego', 'budynek' => '22', 'kodTeryt' => '1811011', 'kodMiejscowosci' => '0974618', 'kodUlicy' => '26464',), 'adresJednostki' => array('wojewodztwo' => 'PODKARPACKIE', 'powiat' => 'mielecki', 'gmina' => 'Mielec', 'miejscowosc' => 'Mielec', 'kodPocztowy' => '39-300', 'rodzaj' => 'ul.', 'ulica' => 'Żeromskiego', 'budynek' => '22', 'kodTeryt' => '1811011', 'kodMiejscowosci' => '0974618', 'kodUlicy' => '26464',), 'numerWpisu' => '00195/18/2017', 'dataWpisu' => '2017-12-19', 'telefon' => '17 780 02 56', 'email' => 'o.gin.pol@szpital.mielec.pl', 'dataObowiazywaniaOd' => '2017-12-19', 'dataObowiazywaniaDo' => '2017-12-19', 'kodSpecjalnosci' => '4450', 'rodzajSpecjalnosci' => 'Oddział położniczo-ginekologiczny', 'miejcaStacjonowania' => array(),), 16 => array('nazwaPodmiotu' => 'SZPITAL POWIATOWY IM.EDMUNDA BIERNACKIEGO W MIELCU', 'nazwaJednostki' => 'Oddział Chirurgii Naczyniowej', 'adresPodmiotu' => array('wojewodztwo' => 'PODKARPACKIE', 'powiat' => 'mielecki', 'gmina' => 'Mielec', 'miejscowosc' => 'Mielec', 'kodPocztowy' => '39-300', 'rodzaj' => 'ul.', 'ulica' => 'Żeromskiego', 'budynek' => '22', 'kodTeryt' => '1811011', 'kodMiejscowosci' => '0974618', 'kodUlicy' => '26464',), 'adresJednostki' => array('wojewodztwo' => 'PODKARPACKIE', 'powiat' => 'mielecki', 'gmina' => 'Mielec', 'miejscowosc' => 'Mielec', 'kodPocztowy' => '39-300', 'rodzaj' => 'ul.', 'ulica' => 'Żeromskiego', 'budynek' => '22', 'kodTeryt' => '1811011', 'kodMiejscowosci' => '0974618', 'kodUlicy' => '26464',), 'numerWpisu' => '00196/18/2017', 'dataWpisu' => '2017-12-19', 'telefon' => '17 780 01 63', 'email' => 'o.chirnaczyniowej@szpital.mielec.pl', 'dataObowiazywaniaOd' => '2017-12-19', 'dataObowiazywaniaDo' => '2017-12-19', 'kodSpecjalnosci' => '4530', 'rodzajSpecjalnosci' => 'Oddział chirurgii naczyniowej', 'miejcaStacjonowania' => array(),), 17 => array('nazwaPodmiotu' => 'SZPITAL POWIATOWY IM.EDMUNDA BIERNACKIEGO W MIELCU', 'nazwaJednostki' => 'Oddział Intensywnej Terapii i Anestezjologii', 'adresPodmiotu' => array('wojewodztwo' => 'PODKARPACKIE', 'powiat' => 'mielecki', 'gmina' => 'Mielec', 'miejscowosc' => 'Mielec', 'kodPocztowy' => '39-300', 'rodzaj' => 'ul.', 'ulica' => 'Żeromskiego', 'budynek' => '22', 'kodTeryt' => '1811011', 'kodMiejscowosci' => '0974618', 'kodUlicy' => '26464',), 'adresJednostki' => array('wojewodztwo' => 'PODKARPACKIE', 'powiat' => 'mielecki', 'gmina' => 'Mielec', 'miejscowosc' => 'Mielec', 'kodPocztowy' => '39-300', 'rodzaj' => 'ul.', 'ulica' => 'Żeromskiego', 'budynek' => '22', 'kodTeryt' => '1811011', 'kodMiejscowosci' => '0974618', 'kodUlicy' => '26464',), 'numerWpisu' => '00197/18/2017', 'dataWpisu' => '2017-12-19', 'telefon' => '17 780 02 43', 'email' => 'o.anestezjologii@szpital.mielec.pl', 'dataObowiazywaniaOd' => '2017-12-19', 'dataObowiazywaniaDo' => '2017-12-19', 'kodSpecjalnosci' => '4260', 'rodzajSpecjalnosci' => 'Oddział anestezjologii i intensywnej terapii', 'miejcaStacjonowania' => array(),),);
//        foreach ($jednostki as $jednostka) {
//            $nazwa = $jednostka['nazwaJednostki'];
////                if (!isset($jednostka['ulica'])) {
////                    var_dump($jednostka);die;
////                }
//
//            $adress = $jednostka['adresPodmiotu'];
//            $adres = ((string)@$adress['ulica']) . ' ' . ((string)@$adress['budynek']) . ', ' . ((string)@$adress['kodPocztowy']) . ' ' . ((string)@$adress['miejscowosc']);
//            $adressForGoogle = 'ul.+' . ((string)@$adress['ulica']) . '+' . ((string)@$adress['budynek']) . ',+' . ((string)@$adress['kodPocztowy']) . '+' . ((string)@$adress['miejscowosc']);
//            $city = ((string)@$adress['miejscowosc']);
////                sleep(1);
//            $curl = curl_init();
//            $url = str_replace(' ', '+', 'https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyD36EpZR-H_zl30MbU0q2C8Vk8L4fkCzd0&address=' . $adressForGoogle);
//            curl_setopt_array($curl, array(
//                CURLOPT_RETURNTRANSFER => 1,
//                CURLOPT_URL => $url
//            ));
////                var_dump(curl_exec($curl));die;
////                curl_init('https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyD36EpZR-H_zl30MbU0q2C8Vk8L4fkCzd0&address=' . $adressForGoogle);
////                $data = file_get_contents(trim('https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyD36EpZR-H_zl30MbU0q2C8Vk8L4fkCzd0&address=' . $adressForGoogle));
//            $d = curl_exec($curl);
//            $location = @json_decode($d, true)['results'][0]['geometry']['location'];
//            $lat = (float)@$location['lat'];
//            $lng = (float)@$location['lng'];
////                var_dump($lat, $lng);die;
//            if (!$lat || !$lng) {
////                var_dump($adressForGoogle);die;
//                $errors[] = $jednostka;
//                continue;
//            }
////                var_dump($lng, $lat);die;
//            $ambulance = new Ambulance();
//            $ambulance
//                ->setName($nazwa)
//                ->setAddress($adres)
//                ->setLng($lng)
//                ->setLat($lat)
//                ->setPhone('+48' . $jednostka['telefon'])
//                ->setCity($city);
//
//            $this->getDoctrine()->getManager()->persist($ambulance);
//            $this->getDoctrine()->getManager()->flush();
//        }
////        }
//        var_export($errors);
//        die;
//        die;
//        foreach ($xml as $x) {
////            $jednostka = $x->jednostka;
////            $adresPodmiotu = $jednostka->adresPodmiotu;
////            $name = $jednostka->nazwaJednostki;
////           $adres = $adresPodmiotu->ulica . ' ' . $adresPodmiotu->budynek . ', ' . $adresPodmiotu->kodPocztowy . ' ' . $adresPodmiotu->miejscowosc;
//            var_dump($x->jednostka->nazwaJednostki);
////            var_dump($x->jednostka);die;
//        }
//        die;
//    }
}
