<?php
/**
 * Created by PhpStorm.
 * User: mateu
 * Date: 24.11.2018
 * Time: 16:43
 */

namespace App\Controller;


use App\Services\BenefitsFetcher;
use App\Services\ICD10EngCodesFetcher;
use App\Services\ICD10PlCodesFetcher;
use App\Services\SymptomsFetcher;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SyncController
 * @package App\Controller
 * @Route("/sync")
 */
class SyncController extends AbstractController
{
    /**
     * @Route("/icd10En")
     */
    public function icd10EnAction() {
        ini_set('max_execution_time', 30000);
        set_time_limit(30000);

        $codesFetcher = $this->get(ICD10EngCodesFetcher::class);
        $codesFetcher->sync();

        return new Response();
    }

    /**
     * @Route("/icd10Pl")
     */
    public function icd10PlAction() {
        ini_set('max_execution_time', 30000);
        set_time_limit(30000);

        $polishCodesNamesFetcher = $this->get(ICD10PlCodesFetcher::class);
        $polishCodesNamesFetcher->sync();

        return new Response();
    }

    /**
     * @Route("/benefits")
     */
    public function benefitsAction() {
        ini_set('max_execution_time', 30000);
        set_time_limit(30000);

        $benefitsFetcher = $this->get(BenefitsFetcher::class);
        $benefitsFetcher->sync();

        return new Response();
    }

    /**
     * @Route("/symptoms")
     */
    public function syncAction() {
        ini_set('max_execution_time', 30000);
        set_time_limit(30000);

        $symptomsFetcher = $this->get(SymptomsFetcher::class);
        $symptomsFetcher->createSymptoms();

        return new Response();
    }

    public static function getSubscribedServices(): array
    {
        return array_merge(parent::getSubscribedServices(), [
            ICD10EngCodesFetcher::class,
            ICD10PlCodesFetcher::class,
            BenefitsFetcher::class,
            SymptomsFetcher::class
        ]);
    }}