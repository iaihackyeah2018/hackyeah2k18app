<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Disease
 * @package App\Entity
 * @ORM\Entity();
 */
class Disease
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var array
     * @ORM\OneToMany(targetEntity="App\Entity\DiseaseName", mappedBy="disease")
     */
    private $names;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $icd10code;

    /**
     * @var Symptom[]
     * @ORM\ManyToMany(targetEntity="App\Entity\Symptom", mappedBy="diseases")
     */
    private $symptoms;

    /**
     * @var Benefit[]
     * @ORM\ManyToMany(targetEntity="App\Entity\Benefit", inversedBy="diseases")
     * @ORM\JoinTable(name="diseases_benefits",
     *   joinColumns={@ORM\JoinColumn(name="disease_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="benefit_id", referencedColumnName="id")})
     */
    private $benefits;

    public function __construct()
    {
        $this->names = new ArrayCollection();
        $this->symptoms = new ArrayCollection();
        $this->benefits = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Disease
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNames()
    {
        return $this->names;
    }

    /**
     * @param mixed $names
     * @return Disease
     */
    public function setNames($names)
    {
        $this->names = $names;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIcd10code()
    {
        return $this->icd10code;
    }

    /**
     * @param mixed $icd10code
     * @return Disease
     */
    public function setIcd10code($icd10code)
    {
        $this->icd10code = $icd10code;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSymptoms()
    {
        return $this->symptoms;
    }

    /**
     * @param mixed $symptoms
     * @return Disease
     */
    public function setSymptoms($symptoms)
    {
        $this->symptoms = $symptoms;
        return $this;
    }

    public function getBenefits()
    {
        return $this->benefits;
    }

    /**
     * @param array $benefits
     * @return Disease
     */
    public function setBenefits(array $benefits): Disease
    {
        $this->benefits = $benefits;
        return $this;
    }
}