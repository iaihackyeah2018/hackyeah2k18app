<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LocationRepository")
 */
class Location
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=7)
     */
    private $lat;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=7)
     */
    private $lng;

    /**
     * Location constructor.
     * @param array $params
     *
     * @throws \Exception
     */
    public function __construct(array $params = [])
    {
        try {
            if (!empty($params)) {
                if (!empty($params['lat'])) {
                    $this->setLat((float)$params['lat']);
                }
                if (!empty($params['lng'])) {
                    $this->setLng((float)$params['lng']);
                }
            }
        } catch (\Throwable $t) {
            throw new \Exception('Wrong latitude or longitude. Float\'s required eg. 50.12345678');
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLat()
    {
        return $this->lat;
    }

    public function setLat(float $lat): self
    {
        $this->lat = $lat;

        return $this;
    }

    public function getLng()
    {
        return $this->lng;
    }

    public function setLng(float $lng): self
    {
        $this->lng = $lng;

        return $this;
    }

    public function setId($id): self
    {
        $this->id = $id;

        return $this;
    }

}
