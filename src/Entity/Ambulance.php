<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AmbulanceRepository")
 */
class Ambulance
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=7)
     */
    private $lat;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=7)
     */
    private $lng;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $phone;

    private $timeTravelData = [];

    public function __construct(array $params = [])
    {
        try {
            if (!empty($params)) {
                if (!empty($params['lat'])) {
                    $this->setLat((float)$params['lat']);
                }
                if (!empty($params['lng'])) {
                    $this->setLng((float)$params['lng']);
                }
                if (!empty($params['name'])) {
                    $this->setName($params['name']);
                }
                if (!empty($params['city'])) {
                    $this->setCity($params['city']);
                }
                if (!empty($params['address'])) {
                    $this->setAddress($params['address']);
                }
                if (!empty($params['phone'])) {
                    $this->setPhone($params['phone']);
                }
            }
        } catch (\Throwable $t) {
            throw new \Exception('Wrong ambulance data');
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLat()
    {
        return $this->lat;
    }

    public function setLat($lat): self
    {
        $this->lat = $lat;

        return $this;
    }

    public function getLng()
    {
        return $this->lng;
    }

    public function setLng($lng): self
    {
        $this->lng = $lng;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     *
     * @return Ambulance
     */
    public function setPhone($phone): self
    {
        $this->phone = $phone;

        return $this;
    }


    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function setTimeTravelData(array $time): self
    {
        $this->timeTravelData = $time;

        return $this;
    }

    public function getTimeTravelData(): array
    {
        return $this->timeTravelData;
    }

    public function setId($id): self
    {
        $this->id = $id;

        return $this;
    }

    public function setColor($color): self
    {
        $this->timeTravelData['color'] = $color;

        return $this;
    }
    public function toArray()
    {
        return [
            'id' => $this->id,
            'lat' => $this->lat,
            'lng' => $this->lng,
            'address' => $this->address,
            'city' => $this->city,
            'name' => $this->name
        ];
    }
}
