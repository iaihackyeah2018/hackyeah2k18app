<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class DiseaseName
 * @package App\Entity
 * @ORM\Entity();
 */
class DiseaseName
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $name;


    /**
     * @var Disease
     * @ORM\ManyToOne(targetEntity="App\Entity\Disease", inversedBy="names")
     * @ORM\JoinColumn(name="disease_id", referencedColumnName="id")
     */
    private $disease;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $language;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return DiseaseName
     */
    public function setId(int $id): DiseaseName
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return DiseaseName
     */
    public function setName(string $name): DiseaseName
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Disease
     */
    public function getDisease()
    {
        return $this->disease;
    }

    /**
     * @param Disease $disease
     * @return DiseaseName
     */
    public function setDisease(Disease $disease): DiseaseName
    {
        $this->disease = $disease;
        return $this;
    }


    /**
     * @return string
     */
    public function getLanguage(): string
    {
        return $this->language;
    }

    /**
     * @param string $language
     * @return DiseaseName
     */
    public function setLanguage(string $language): DiseaseName
    {
        $this->language = $language;
        return $this;
    }
}