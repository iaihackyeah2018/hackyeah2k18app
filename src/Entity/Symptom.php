<?php
/**
 * Created by PhpStorm.
 * User: mateu
 * Date: 24.11.2018
 * Time: 14:49
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Symptom
 * @package App\Entity
 * @ORM\Entity();
 */
class Symptom
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var Disease[]
     * @ORM\ManyToMany(targetEntity="App\Entity\Disease", inversedBy="symptoms")
     * @ORM\JoinTable(name="symptoms_diseases")
     */
    private $diseases;

    /**
     * @var Benefit[]
     * @ORM\ManyToMany(targetEntity="App\Entity\Benefit", inversedBy="symptoms")
     * @ORM\JoinTable(name="symptoms_benefits")
     */
    private $benefits;

    public function __construct() {
        $this->diseases = new ArrayCollection();
        $this->benefits = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Symptom
     */
    public function setName(string $name): Symptom
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Symptom
     */
    public function setId(int $id): Symptom
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Disease[]
     */
    public function getDiseases(): array
    {
        return $this->diseases;
    }

    /**
     * @param Disease[] $diseases
     * @return Symptom
     */
    public function setDiseases(array $diseases): Symptom
    {
        $this->diseases = $diseases;
        return $this;
    }


    public function getBenefits()
    {
        return $this->benefits;
    }

    /**
     * @param array
     * @return Symptom
     */
    public function setBenefits(array $benefits): Symptom
    {
        $this->benefits = $benefits;
        return $this;
    }
}