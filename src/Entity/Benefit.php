<?php
/**
 * Created by PhpStorm.
 * User: mateu
 * Date: 24.11.2018
 * Time: 23:59
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Benefit
 * @package App\Entity
 * @ORM\Entity();
 */
class Benefit
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Symptom", mappedBy="benefits")
     */
    private $symptoms;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Disease", mappedBy="benefits")
     */
    private $diseases;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Benefit
     */
    public function setId(int $id): Benefit
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Benefit
     */
    public function setName(string $name): Benefit
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return array
     */
    public function getSymptoms(): array
    {
        return $this->symptoms;
    }

    /**
     * @param array $symptoms
     * @return Benefit
     */
    public function setSymptoms(array $symptoms): Benefit
    {
        $this->symptoms = $symptoms;
        return $this;
    }

    /**
     * @return array
     */
    public function getDiseases(): array
    {
        return $this->diseases;
    }

    /**
     * @param array $diseases
     * @return Benefit
     */
    public function setDiseases(array $diseases): Benefit
    {
        $this->diseases = $diseases;
        return $this;
    }
}