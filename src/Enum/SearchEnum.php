<?php
/**
 * Created by PhpStorm.
 * User: mateu
 * Date: 25.11.2018
 * Time: 01:24
 */

namespace App\Enum;


class SearchEnum
{
    const SYMPTOM = 1;
    const DISEASE = 2;
}