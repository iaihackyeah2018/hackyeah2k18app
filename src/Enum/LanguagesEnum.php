<?php
/**
 * Created by PhpStorm.
 * User: mateu
 * Date: 24.11.2018
 * Time: 19:05
 */

namespace App\Enum;


class LanguagesEnum
{
    const POLISH = "pl";
    const ENGLISH = "en";
}