<?php
/**
 * Created by PhpStorm.
 * User: mateu
 * Date: 25.11.2018
 * Time: 06:38
 */

namespace App\Enum;


class ModeEnum
{
    const MAP = "map";
    const LIST = "list";
}