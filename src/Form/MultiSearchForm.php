<?php
/**
 * Created by PhpStorm.
 * User: mateu
 * Date: 25.11.2018
 * Time: 00:49
 */

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class MultiSearchForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('phrase', TextType::class)
            ->add('submit', SubmitType::class)
        ;
    }
}