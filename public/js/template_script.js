var emergencytime = function() {
    var dialog = document.getElementById('emergency-dialog');

    if (dialog) {
        dialog.show();
    } else {
        ons.createElement('emergency_dialog.html', { append: true })
            .then(function(dialog) {
                dialog.show();
            });
    }
};

var login_dialog = function() {
    var dialog = document.getElementById('login-dialog');

    if (dialog) {
        dialog.show();
    } else {
        ons.createElement('login_dialog.html', { append: true })
            .then(function(dialog) {
                dialog.show();
            });
    }
};

var menu_dialog = function() {
    var dialog = document.getElementById('menu-dialog');

    if (dialog) {
        dialog.show();
    } else {
        ons.createElement('menu_dialog.html', { append: true })
            .then(function(dialog) {
                dialog.show();
            });
    }
};

var login = function() {
    var username = document.getElementById('username').value;
    var password = document.getElementById('password').value;

    if (username === 'admin' && password === 'admin') {
        ons.notification.alert('Zalogowano jako ' + username);
    } else {
        ons.notification.alert('Incorrect username or password.');
    }
};


var hideDialog = function(id) {
    document
        .getElementById(id)
        .hide();
};

window.fn = {};

window.fn.open = function() {
    var menu = document.getElementById('menu');
    menu.open();
};

window.fn.load = function(page) {
    var content = document.getElementById('content');
    var menu = document.getElementById('menu');
    content.load(page)
        .then(menu.close.bind(menu));
};

