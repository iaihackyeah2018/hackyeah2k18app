// szpachlakod lokalizacje
if (!Cookies.get('lat') || typeof Cookies.get('lat') === undefined || Cookies.get('lat') === 'undefined')
    var x,y, town, state, city;
var currentPosition = null;
var getCityByPosition = function (options) {
    return new Promise(function (resolve, reject) {
        jQuery.post( "https://nominatim.openstreetmap.org/reverse?format=json&lat=" + currentPosition.lat + "&lon=" + currentPosition.lng + "&zoom=18&addressdetails=1", function(success) {
            resolve(success)
        }).fail(function(err) {
            reject(err);
        });
    });
};
var getPosition = function (options) {
    return new Promise(function (resolve, reject) {
        jQuery.post( "https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyD36EpZR-H_zl30MbU0q2C8Vk8L4fkCzd0", function(success) {
            resolve(success)
        }).fail(function(err) {
            reject(err);
        });
    });
};
getPosition()
    .then((position) => {
        currentPosition = {
            lat: position.location.lat,
            lng: position.location.lng
        };
        x = position.location.lat;
        y = position.location.lng;
    })
    .catch((err) => {
        alert("Cannot check your current location!");
    });
var waitForCurrentPosition = setInterval(function () {
    if (currentPosition !== null) {
        getCityByPosition()
            .then((position) => {
                console.log(position);
                city = position.address.city;
                town = position.address.town;

                state = position.address.state;

                Cookies.set('lat', x);
                Cookies.set('lng', y);
                Cookies.set('city', city);
                Cookies.set('town', town);
                Cookies.set('state', state);
            })
            .catch((err) => {
                alert("Cannot check your current location!");
            });
        clearInterval(waitForCurrentPosition);
    }
});