var mymapp = null;
var yourMarkers = {};
var greenIcon = new L.Icon({
    iconUrl: markersColor.green,
    shadowUrl: markerShadow,
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});
var redIcon = new L.Icon({
    iconUrl: markersColor.red,
    shadowUrl: markerShadow,
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});
var blueIcon = new L.Icon({
    iconUrl: markersColor.blue,
    shadowUrl: markerShadow,
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});
var yellowIcon = new L.Icon({
    iconUrl: markersColor.yellow,
    shadowUrl: markerShadow,
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});

function loadMap(container) {
    // console.log(container);
    mymapp = L.map(container).setView([ currentPosition.lat, currentPosition.lng ], 11);
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox.streets',
        accessToken: 'pk.eyJ1IjoibXBlZHppayIsImEiOiJjam92bnI2cjUwNnVuM3FwY2x2eWVmZThyIn0.t2jB5gYtshIU-TYYIcStHA'
    }).addTo(mymapp);
    L.marker([ currentPosition.lat, currentPosition.lng], {icon: yellowIcon}).addTo(mymapp).bindPopup('<h1>Twoja lokalizacja</h1>');
}

function addMarker(data, name) {
    //Można rozbudować marker o inne informacje
    let coors = [
        data.lat, data.lng
    ];
    var iconn = blueIcon;
    if (data.hasOwnProperty('color') && markersColor.hasOwnProperty(data.color)) {
        switch (data.color) {
            case 'blue' :
                iconn = blueIcon;
                break;
            case 'red' :
                iconn = redIcon;
                break;
            case 'yellow' :
                iconn = yellowIcon;
                break;
            case 'green':
                iconn = greenIcon;
                break;
        }
    }
    yourMarkers[name] = L.marker(
        coors,
        {icon: iconn}
    ).addTo(mymapp);
    if (data.hasOwnProperty('onClick')) {
        yourMarkers[name].on('click', data.onClick);
    }
    if (data.hasOwnProperty('popup')) {
        yourMarkers[name].bindPopup(data.popup);
    }
}